Urban spaces are hotspots of environmental pollution such as noise, air
pollution and heat. These stressors affect our health and are highly
contextually distributed in space and time. Portable sensors have advanced the
measurement of these stressors and allow mobile exposure measurements.

Environmental-related Volunteered Geographic Information (VGI), which is
actively collected by non-experts, can thus simultaneously provide valuable
knowledge about temporal-spatial exposures and the mobility behaviour of
individuals. Smartphone-based detection methods also provide active options to
capture users' perceptions and opinions about exposure. This combination of
subjective and objective VGI is a comprehensive data pool that not only allows
exposure to be evaluated but also the behaviour and decision motives of
individuals during everyday mobility.

In this project, we therefore work on three main questions:
1. How can environmental VGI generated with smartphones be used to extract spatiotemporal movement patterns and exposures?
2. How can we collect subjective data and analyse behavioural changes when confronting people with exposure data?
3. How can environmental VGI be used to improve mobility and exposure models?

We use intelligent sensors and an interventional study design, based on a
self-developed framework of contemporary geography and psychological theories,
to collect VGI with individuals from different socio-economic contexts. We will
analyse the data using GIS-based methods of space-time analysis and
psychological test statistics. To compare empirical data with models, we perform
agent-based simulations.

The main goal of this project is to explore the potential of environmental VGI
to change mobility behaviour in line with a healthier pathway decision by
raising awareness of exposure.

In the following we give an overview of the results of the project.

## Mobile Sensors (Wearables)

City dwellers and participants in walking and cycling are exposed to
environmental stressors. In our studies we used mobile sensors that can be worn
by citizens to measure their exposure to environmental stressors. Within two
measurement campaigns the cyclists and pedestrians have taken the sensors on
their daily routes through the city of Leipzig.

The first measurement campaign took place from July 2020 to October 2020. A set
of sensors was used here, consisting of the fine dust sensor Dylos
DC1700 (PM2.5, PM10), the gas (NO2, NO, O3) and Leo/Ateknea temperature sensor,
and a smartphone (noise, light, GPS, time stamp) (see photo). In the second
measurement campaign from August 2021 to August 2022, the PAM (Personal Air
Quality Monitor) sensor (PM1, PM2.5, PM10, NO2, NO, CO, O3, temperature,
humidity, noise, GPS, time stamp) was used in combination with a mobile app (see
below).

![](./images/image1.png)  
*Figure 1: Sensor set of first measurement campaign*

![](./images/image2.jpg)  
*Figure 2: Sensor of second measurement campaign*

## Mobile App

For the second measurement campaign, a progressive web app (PWA) was developed.
The technology was used because of its accessibility via any browser or easy
installation on all smartphones as well as a relatively low development effort
(compared to native apps). The app was used by the study participants to track
the start and end of their routes as well as to collect answers about the route.
In addition, participants were able to view their measured tracks after
uploading the sensor data. Another feature of the app was that the participants
were shown an alternative route, less exposed by environmental stressors, which
they could then drive the next time.

![](./images/image3.jpg)  
*Figure 3: PWA for tracking the routes of participants*

## Measurement Campaign

In two field experiments, we not only equipped cyclists and pedestrians with our
sensor technology, but also used questionnaires to study their perceptions of
environmental stressors as well as their intentions to change their routes in
order to avoid pollution.

In both studies participants were allocated to a measurement group or a control
group. The measurement group used the wearable sensors for three days and
received an individualized feedback report at the end (study 1) or everyday
feedback in a mobile app including alternative route suggestions (study 2). The
control group did not carry the sensors or receive feedback, while both groups
filled out multiple questionnaires throughout the study period. This procedure
allowed us to identify causal effects of using the sensor kit and receiving
feedback.

In the first study a final sample of 107 participants finished each
questionnaire of our study. In the second study the final sample consisted of
137 participants.

In the questionnaires, we asked participants about psychological concepts, that
can help to explain health behaviour. These predictors of healthy behaviour were
based on Protection Motivation Theory (PMT, Rogers, 1975). PMT predicts healthy
behaviour change, when threat perceptions (perceiving the current situation as a
health hazard with severe consequences) as well as coping appraisals (capacity
to adapt behaviours to lower the health hazard) are high.

Using a wearable sensor and receiving feedback on exposure levels makes health
hazards such as particulate matter visible to the user. We tested, whether
making these stressors visible, would affect participant's threat perceptions
and willingness to choose less polluted routes in the future.

![](./images/image4.jpg)  
*Figure 4: PAM sensors prepared for the measurement campaign*

![](./images/image5.jpg)  
*Figure 5: Instruction of a participant in the sensor technology*

![](./images/image6.jpg)  
*Figure 6: Effect on Threat perceptions in study 1 and study 2*

In both study 1 and study 2, we found that health threat perceptions for
particulate matter increased in the measurement group, while they did not
increase in the control group (Figure 6). This is an indication that wearable
sensors have a psychological effect on the people wearing them.

Effects were less consistent when looking at participant's intentions to change
their routing behaviour. In general, effects of wearing the sensors had
relatively short-termed effects on people's intentions to take less polluted
routes. Specifically, wearing the sensors increased intentions to take less
polluted routes from pretest to posttest in study 1, albeit only for
participants who had no strong habits for their routing choices at the start of
the study (Figure 7).

In study 2 we found that participants in the measurement group increased their
intentions to change their routing behaviour slightly more than those in the
control group (Figure 8, marginally signifficant).

![](./images/image7.jpg)  
*Figure 7: Effects on Intentions to change in study 1*

![](./images/image8.jpg)  
*Figure 8: Effects on Intentions to change in study 2*

## Visualisation Application

We implemented a prototype of a 3D visualization and analysis application with
which the distribution of stressors (hot and cold spots) within the city can be
analysed and evaluated. The application is based on the Unity game engine, a
development environment for computer games and other interactive 3D graphics
applications. Using Unity enables us to combine methods of Visual Analytics, 3D
visualisation, and Virtual Reality as well as to implement analysis methods and
make them available via a user interface. For further details on and the
download of the application see Helbig et al. 2021.

![](./images/image9.png)  
*Figure 9: 3D application for exploring and analysing the mobile sensor data*
